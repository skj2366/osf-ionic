import { Component, OnInit } from '@angular/core';
import { CommonService } from '../common/common.service';
import { Member } from '../vo/member';
import { ThrowStmt } from '@angular/compiler';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  members:Member[];
  baseImgUrl:string = 'http://192.168.0.18:88/img/';
  constructor(private cs:CommonService,private ac:AlertController, private router:Router) { }

  ngOnInit() {
    this.doSelectMember();
  }

  doSelectMember(){
    this.cs.get('/members').subscribe(
      res=>{
        this.members = <Member[]>res;
      },
      err=>{
        console.log(err);
      }
    )
  }

  async doDelete(omNo:number){
    const alter1 = await this.ac.create({
      header : '경고',
      message : '삭제할꺼니?',
      buttons : [
        {
          text: '예',
          handler : () =>{
            this.cs.delete('/member/' + omNo).subscribe(
              res=>{
                if(res===1){
                  console.log('doDelete result : ' + res);
                  alter1.dismiss();
                  alert('삭제되었습니다');
                  this.doSelectMember();
                }else{
                  alter1.dismiss();
                }
              },
              err=>{
                console.log(err);
              }
            )
          }
        },
        {
          text: '아니요'
        }
      ]
    })
    await alter1.present();
  }

  doModify(omId:string){
    this.router.navigateByUrl('/tabs/signup/' + omId);
  }

}
