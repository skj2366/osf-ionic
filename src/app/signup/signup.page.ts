import { Component, OnInit } from '@angular/core';
import { Member } from '../vo/member';
import { CommonService } from '../common/common.service';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {
  member:Member = new Member();
  isUnique:boolean = false;
  isModify:boolean = false;
  option={};
  btnStr : string ='SignUp';
  constructor(private cs:CommonService, private _http:HttpClient,
    private route:ActivatedRoute,
    private lc:LoadingController,
    private router:Router) {

  }

  async ngOnInit() {
    var omId = this.route.snapshot.paramMap.get('omId');
    console.log(omId);
    if(this.route.snapshot.paramMap.get('omId')){
      this.isModify = true;
      this.btnStr = 'Modify';
      var lcv = await this.lc.create({
        message: '불러오는중..'
      })
      await lcv.present();
      this.cs.get('/member/'+this.route.snapshot.paramMap.get('omId')).subscribe(
        res=>{
          console.log(res);
          if(res){
            this.member = <Member>res;
            this.member.omProfile = 'http://localhost:88/img/' + this.member.omProfile;
            // document.getElementById('checkIdBtn').disabled =true;
            // document.getElementById('inputId').disabled =true;  
          }else{

          }
          lcv.dismiss();
        },
        err=>{
          lcv.dismiss();
        }
      )
    }
  }

  setDaumAddressApi(evt){
    this.member.omZipcode = evt.zip;
    this.member.omAddr1 = evt.addr;
  }

  async doJoin(){
    if(!this.isModify){
      if(this.isUnique==false){
        alert('아이디를 확인하세요');
        return false;
      }else{
        this.member.omBirth = this.member.omBirth.split('T')[0].split('-').join('');
        var url = "/member";
        var lcv = await this.lc.create({
          message: '회원가입 중 ....'
        })
        await lcv.present();
        this.cs.postFile(url,this.member).subscribe(
          res=>{
            console.log(res);
            lcv.dismiss();
            alert('회원가입 성공');
            //this.router.navigateByUrl('/tabs/home');
            location.href = '/tabs/home';
          },
          err=>{
            console.log(err);
            lcv.dismiss();
            alert('회원가입 실패');
          }
        )
      }
    }else{
      this.member.omBirth = this.member.omBirth.split('T')[0].split('-').join('');
      var url = '/member/modi';
      var lcv = await this.lc.create({
        message: '수정 중 ....'
      })
      await lcv.present();
      if(!this.member.omProfileFile){
        // alert('파일이없습니다');
        // return;
        this.cs.postJson(url,this.member).subscribe(
          res=>{
            console.log(res);
            lcv.dismiss();
            alert('수정 성공');
            //this.router.navigateByUrl('/tabs/home');
            location.href='/tabs/home';
          },
          err=>{
            console.log(err);
            lcv.dismiss();
            alert('수정 실패');
          }
        )
        return;
      }
      this.cs.postFile(url,this.member).subscribe(
        res=>{
          console.log(res);
          lcv.dismiss();
          alert('수정 성공');
          //this.router.navigateByUrl('/tabs/home');
          location.href='/tabs/home';
        },
        err=>{
          console.log(err);
          lcv.dismiss();
          alert('수정 실패');
        }
      )
    }
    console.log(this.member);
  }

  setFile(evt){
    //this.member.omProfileFile = evt.target.files[0];
    var reader = new FileReader();
    reader.onload = (e)=>{
      this.member.omProfile = 
        (<FileReader>e.target).result.toString();
    }
    reader.readAsDataURL(evt.target.files[0]);
    this.member.omProfileFile = evt.target.files[0];
  }

  checkId(){
    var url = "/member/" + this.member.omId;
    this.cs.get(url).subscribe(
      res=>{
        if(!res){
          var con_id = confirm(`${this.member.omId}는 사용 가능한 아이디입니다. 사용하시겠습니까?`);
          if(con_id){
            // document.getElementById('checkIdBtn').disabled =true;
            // document.getElementById('inputId').disabled =true;  
            this.isUnique = true;
          }          
        }else{
          alert(`${this.member.omId}는 사용 중복된 아이디입니다.`);
          this.isUnique=false;
        }
      },
      err=>{
        console.log(err);
      }
    )
  }
}
